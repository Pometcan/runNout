
---

## English

Although I have wanted to work on this project for years, it has been left incomplete due to my inability to allocate time. I assure you that I will complete this project when I have the opportunity. I apologize in advance for this delay and will make up for it.

---

## Türkçe

Yıllardır bu projeye çalışmak istemiş olsam da, zaman ayıramadığım için yarım kalmıştır. Bu projeyi uygun bir fırsat bulduğumda tamamlayacağımdan emin olabilirsiniz. Bu gecikme için şimdiden özür dilerim ve telafi edeceğim.

---
